//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var requestjson = require('request-json');

var path = require('path');

var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/ncroce/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlmovimientosMLab);

var movimientosJSONv2 = require("./movimientosv2.json");

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', (req, res)=> {
  res.status(200).sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', (req, res) => {
  res.status(200).send('Hemos recibido su petición POST cambiada!');
});

app.get('/clientes/:idCliente', (req, res)=> {
  res.status(200).send('Aquí tiene el cliente número: ' + req.params.idCliente);
});

app.get('/movimientos', (req, res)=> {
  clienteMLab.get('', (err, resM, body)=> {
    if(err) {
      console.log(body);
    } else {
      res.status(200).send(body);
    }
  });
});

app.post('/movimientos', (req, res)=> {
  clienteMLab.post('', req.body, (err, resM, body)=> {
    if(err) {
      console.log(body);
    } else {
      res.status(200).send(body);
    }
  });
});

app.get('/v2/movimientos', (req, res)=> {
  res.status(200).send(movimientosJSONv2);
});

app.get('/v2/movimientos/:id', (req, res)=> {
  res.status(200).send(movimientosJSONv2.filter(el => el.id == req.params.id ));
});

app.get('/v2/movimientosq', (req, res)=> {
  res.status(200).send(movimientosJSONv2.filter(el => el.id == req.query.id ));
});

app.post('/v2/movimientos', (req, res) => {
  const nuevo = req.body;
  nuevo.id = movimientosJSONv2.length +1;
  movimientosJSONv2.push(nuevo);
  res.status(200).send("Se agregó el elemento:" + JSON.stringify(nuevo) + "\n EN \n"  +  JSON.stringify(movimientosJSONv2));
})
